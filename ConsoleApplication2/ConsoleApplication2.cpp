// ConsoleApplication2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>

const int n = 5;

int main()
{
	int a[n][n] =
	{
		{ 2,5,6,1,4 },
		{ 8,9,1,3,5 },
		{ 1,2,3,4,5 },
		{ 1,0,1,0,1 },
		{ 1,2,6,9,3 }
	};
	int b[n][n] =
	{
		{ 7,2,4,1,4 },
		{ 9,4,6,2,1 },
		{ 1,1,1,1,1 },
		{ 2,2,2,2,2 },
		{ 7,3,2,4,1 }
	};
	int sum[n][n];
	for (int i = 0; i<n; i++)
	{
		for (int j = 0; j<n; j++)
		{
			sum[i][j] = a[i][j] + b[i][j];
			std::cout << std::setw(3) << sum[i][j];
		}
		std::cout << '\n';
	}
	return 0;
}

